module Flyer where
import Data.List

type Miles = Float
type Segments = Int
type Dollars = Float

data Status = Silver | Gold | Platinum | Diamond deriving (Show, Eq)

meetsSilver :: Miles -> Segments -> Dollars -> Maybe Status
meetsSilver m s d
    | (m >= 25000 || s >= 30) && d >= 2500 = Just Silver
    | otherwise = Nothing

meetsGold m s d
    | (m >= 50000 || s >= 60) && d >= 5000 = Just Gold
    | otherwise = Nothing
meetsPlatinum m s d
    | (m >= 75000 || s >= 100) && d >= 7500 = Just Platinum
    | otherwise = Nothing
meetsDiamond m s d
    | (m >= 125000 || s >= 140) && d >= 125000 = Just Diamond
    | otherwise = Nothing


safeHead [] = Nothing
safeHead (x:xs) = Just x

determineStatus :: Miles -> Segments -> Dollars -> Maybe Status
determineStatus m s d =
    let  fs = [meetsDiamond, meetsPlatinum, meetsGold, meetsSilver]
    in case safeHead $ dropWhile (== Nothing) $ map (\f -> f m s d) fs of
       Nothing -> Nothing
       Just m -> m
