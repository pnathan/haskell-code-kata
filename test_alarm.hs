module TestAlarm where

import Data.Set
import Test.HUnit
import Alarm


data SensorMockLow = LowSensor
instance SensorIfc SensorMockLow where
    sampleSensor s = do (return 10)

data SensorMockHi = HiSensor
instance SensorIfc SensorMockHi where
    sampleSensor s = do (return 10)

isLow = TestCase ( do
                   a <- (isAlarmOn (tireAlarm LowSensor ))
                   assertEqual "is low" a True)
isNotLow = TestCase ( do
                   a <- (isAlarmOn (tireAlarm HiSensor ))
                   assertEqual "is not low" a False)

main = runTestTT $ TestList [ isLow ]
