module TestSanta where

import Data.Set
import Test.HUnit
import Santa


withEmptySantas = TestCase ( assertEqual
                             "WithEmptySantas"
                             (generateSantas [])
                                 Nothing )

withMinSantasFail = TestCase ( assertEqual
                               "WithMinSantasFail"
                               (generateSantas ["1", "2", "3"])
                                   Nothing)

-- probably should be set-equal
withMinSantasEqual = TestCase ( assertEqual "WithMinSantasEqual"
                                (Just (fromList
                                       [("1", "4"),
                                        ("2", "1"),
                                        ("3", "2"),
                                        ("4", "3")]))
                                (generateSantas ["1", "2", "3", "4"]))

main = runTestTT ( TestList [ withEmptySantas, withMinSantasFail, withMinSantasEqual])
