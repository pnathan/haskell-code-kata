module Santa where
import Data.List
import Data.Set

type Santa = String

rotate n xs = take (length xs) (drop n (cycle xs))

generateSantas :: [Santa] -> Maybe (Set (Santa, Santa))
generateSantas p =
    if (length p) < 4 then
        Nothing
    else
        Just (fromList (zip b a)) where
            a = p
            b = rotate 1 p
