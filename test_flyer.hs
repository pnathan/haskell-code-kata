module TestFlyer where

import Data.Set
import Test.HUnit
import Flyer

isNothing = TestCase $ assertEqual "isSilver"  Nothing $ determineStatus 0 0 0

isSilver = TestCase $ assertEqual "isSilver" (Just Silver) $ determineStatus 26000.0 31 6000.0
isAlmostGoldByMiles = TestCase $ assertEqual "isAlmostGoldByMiles" (Just Silver) $ determineStatus 10.0 60 4999.9
isBarelyGoldBySegments = TestCase $ assertEqual "isBarelyGoldBySegments" (Just Gold) $ determineStatus 10.0 61 5000.1
isBarelyGoldByMiles = TestCase $ assertEqual "isBarelyGoldByMiles" (Just Gold) $ determineStatus 50000.1 1 5000.1
isReallyNotFrequentButSpentLots = TestCase $ assertEqual "isReallyNotGoldButSpentLots" (Nothing) $ determineStatus 300 1 50000.1

main = runTestTT $ TestList [ isNothing, isSilver, isAlmostGoldByMiles, isBarelyGoldByMiles, isBarelyGoldBySegments, isReallyNotFrequentButSpentLots]
