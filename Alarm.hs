module Alarm where
import System.Random

class SensorIfc a where
    sampleSensor :: a -> IO Int

data Sensor = PhysicalSensor

data AlarmSensor a = Alarm Int Int a

instance SensorIfc Sensor where
    sampleSensor s = randomRIO (14, 100 :: Int)

isAlarmOn  :: SensorIfc a => AlarmSensor a -> IO Bool
isAlarmOn (Alarm low high s) =
    do
      pressure <- sampleSensor s
      if pressure < low || pressure > high then (return True) else (return False)

tireAlarm :: (SensorIfc a) => a -> AlarmSensor a
tireAlarm sensor = Alarm 17 21 sensor
